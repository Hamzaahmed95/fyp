var express = require('express');
var router = express.Router();
var model = require("../bin/models");
var constants = require("../config/constants");
var requestHelper = require("../helpers/request");
var responseHelper = require("../helpers/response");
var url = require('url');



router.post('/login', function(req, res){
    postBody = requestHelper.parseBody(req.body);
    var post = postBody.post;
    model.Signup.find({where: {username:postBody.username, password:postBody.password}}).then(function (Signups) {

        if(Signups == null)
        {
            res.statusCode = 404;
            res.send(Signups);
        }
        else
        {
            return res.json(Signups);
        }
    })
});


router.post('/loginuser', function(req, res){
    postBody = requestHelper.parseBody(req.body);
    var post = postBody.post;
    model.user.find({where: {username:postBody.username, password:postBody.password}}).then(function (users) {

        if(users == null)
        {
            res.statusCode = 404;
            res.send(users);
        }
        else
        {
            return res.json(users);
        }
    })
});

router.post('/login', function(req, res){
    postBody = requestHelper.parseBody(req.body);
    var post = postBody.post;
    model.user.find({where: {username:postBody.username, password:postBody.password}}).then(function (users) {

        if(users == null)
        {
            res.statusCode = 404;
            res.send(users);
        }
        else
        {
            return res.json(users);
        }
    })
});


router.post('/signup',function (req, res) {
    postBody = requestHelper.parseBody(req.body);
    model.Signup.create({
        firstName: postBody.firstName,

        lastName: postBody.lastName,
        email: postBody.email,

        username : postBody.username,
        password: postBody.password,
    })

    res.send("Sign Up Successfully!");

});



router.delete('/Deleteuser', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.query.id;

    model.Signup.destroy({where: {id:id}}).then(function (Signups) {
        res.json(Signups)
    })
});

router.put('/UpdateStudent',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.query.id;

    model.Signup.find({where: {id:id}}).then(function (Signups) {
        return Signups.updateAttributes({ firstName:req.body.firstName,lastName:req.body.lastName,
            email:req.body.email, username:req.body.username, password:req.body.password});
    }).then(function (Signups) {
        res.json(Signups);
    })
});



module.exports = router;

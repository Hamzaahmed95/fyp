
var bodyParser = require('body-parser');
var express = require('express');
var router = express.Router();
var model = require("../bin/models");
var constants = require("../config/constants");
var requestHelper = require("../helpers/request");
var responseHelper = require("../helpers/response");
var url = require('url');
var jwt = require('jsonwebtoken');

var config = require('../bin/config/config.json');
var bcrypt = require('bcryptjs');


router.post('/authenticate', function(req, res) {
    model.Loginapi.findOne({where: { email: req.body.email }}).then(function(Loginapi) {

        if (!Loginapi) {

            res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (Loginapi) {

            // check if password matches
            if (Loginapi.password != req.body.password) {
                res.json({ success: false, message: 'Authentication failed. Wrong password.' });
            } else {

                var token = jwt.sign({
                    data: 'foobar'
                }, 'secret', { expiresIn: '1h' });




//              model.Loginapi.findOne({where: {email: req.body.email}, attributes: ['role']}, att).then(function(_user) {
               res.json({
                        success: true,
                        message: 'Enjoy your token!',
                        token: token,
                       //user: _user,
                    });

  //             });
            }

        }

    }).catch(function(err) {
        throw new Error(err);
    });
});



router.post('/createusers',function (req, res) {

    var password = bcrypt.hashSync(req.body.password, 8);
    model.Loginapi.create({
            email : req.body.email,
            password : password,
            role : req.body.role,

        },
        function (err, Loginapi) {
            if (err) return res.status(500).send("There was a problem registering the user.")
            // create a token
            var token = jwt.sign({ id: Loginapi._id }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            res.status(200).send({ auth: true, token: token });
        });
});

router.get('/me', function(req, res) {
    var token = req.headers['x-access-token'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

    jwt.verify(token, config.secret, function(err, decoded) {
        if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

        res.status(200).send(decoded);
    });
});


router.post('/createuser',function (req, res) {
    postBody = requestHelper.parseBody(req.body);

    model.Loginapi.create({
        email: postBody.email,
        password: postBody.password,

        role : postBody.role
           })

    res.json({ success: true, message: 'User Create Successfully.' });
});



router.delete('/Deleteuser', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.query.id;

    model.Loginapi.destroy({where: {id:id}}).then(function (Loginapis) {
        res.json(Loginapis)
    })
});

router.put('/Updateuser',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.query.id;

    model.Loginapi.find({where: {id:id}}).then(function (Loginapis) {
        return Loginapis.updateAttributes({
            email:req.body.email,  password:req.body.password,role:req.body.role});
    }).then(function (Loginapis) {
        res.json(Loginapis);
    })
});

router.get('/productname', function(req, res, next) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var productname = req.query.productname;

    model.product.find({where:{productname:productname}}).then(function(loginapis){
        res.json(loginapis);
    });
});


router.post('/adduser',function (req, res) {
    postBody = requestHelper.parseBody(req.body);

    model.Loginapi.find({where:{
        email:postBody.email,
        password:postBody.password,
        }}).then(function(Loginapis){

        if(Loginapis == null) {
            model.Loginapi.create({
                email: postBody.email,
                password: postBody.password,
                role:postBody.role

            })
            res.json({ success: true, message: 'User Create Successfully.' })
        }

        else {res.json({ success: false, message: 'User already exist.' })
        }})})


module.exports = router;

//model.loginapi.find({ role:"admin" })



//Sequelize.tablename.find({key:value})
'use strict';
module.exports = function(sequelize, DataTypes) {
  var Loginapi = sequelize.define('Loginapi', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Loginapi;
};